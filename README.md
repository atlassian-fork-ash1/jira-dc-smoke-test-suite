# Dependencies

## [node.js/npm](https://nodejs.org/en/)

Tested with node version 11.6.0 and npm version 6.5.0

## ChromeDriver

**Mac OS**: Can be installed via `brew tap homebrew/cask && brew cask install chromedriver`

**Linux: RPM**: Can be installed via:
```
sudo yum -y install libX11
wget -N https://chromedriver.storage.googleapis.com/2.35/chromedriver_linux64.zip -P ~/
unzip ~/chromedriver_linux64.zip -d ~/
rm ~/chromedriver_linux64.zip
sudo mv -f ~/chromedriver /usr/local/bin/chromedriver
sudo chown root:root /usr/local/bin/chromedriver
sudo chmod 0755 /usr/local/bin/chromedriver
```

# Developing

Run the linter before committing your changes: `npm run lint`. The linter will spit out warnings for unused expressions because some chai assertions look like they don't do anything.
The only time these warnings are acceptable are for calls to chai assertions which terminate with a property, not a function such as `expect(foo).to.be.true`.

# Execution

Easy! Set the following environment variables:

|Variable|Description|
|--------|:---------:|
|`JIRA_BASE_URL`|The url of the Jira instance under test (include scheme i.e. http/https)|
|`JIRA_ADMIN`|Username of the admin account for the Jira instance under test|
|`JIRA_LICENSE_KEY`|The license to install with this Jira DC instance|
|`JIRA_ADMIN_PWD`|Password of the admin account for the Jira instance under test|
|`JIRA_USER`|Username of a normal user for the Jira instance under test|
|`JIRA_USER_PWD`|Password of the normal user for the Jira instance under test|

Then run the following commands:

- `npm install`
- `npm test`

