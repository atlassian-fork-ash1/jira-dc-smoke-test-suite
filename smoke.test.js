const { Builder, until } = require('selenium-webdriver');
const chrome = require('selenium-webdriver/chrome');
const { Key } = require('selenium-webdriver/lib/input')
const { expect } = require('chai');
const { before, afterEach, after, describe, it } = require('mocha');
const selectors = require('./selectors');

const { JIRA_ADMIN, JIRA_ADMIN_PWD, JIRA_BASE_URL, JIRA_USER, JIRA_USER_PWD } = process.env;
const TEST_NONCE = Math.floor(Math.random() * 10000);

console.log(`Testing Jira at: ${JIRA_BASE_URL}`)

const DEBUG = process.argv.includes('--debug-selenium');
if (DEBUG) console.log('In debug mode');

function createWebDriver() {
  let opts = new chrome.Options()
                       .headless()
                       .addArguments('--no-sandbox')
                       .windowSize({ height: 1920, width: 1080 });

  if (DEBUG) opts = opts.addArguments('--remote-bugging-port=9222');

  return new Builder()
    .forBrowser('chrome')
    .setChromeOptions(opts)
    .build();
}

describe('Jira Data Center', function() {
  before(async function() {
    this.driver = await createWebDriver();
    this.driver.manage().setTimeouts({ implicit: 1000 });
    this.timeout(300000)

    // Wait a couple minutes for Jira to be ready
    await this.driver.get(JIRA_BASE_URL);
    await this.driver.wait(until.elementLocated(selectors.DASHBOARD_LOGIN_FORM_USERNAME), 300000, 'Jira is not ready for testing. Wait a few minutes and try again');
  });

  afterEach(async function() {
    await this.driver.manage().deleteAllCookies();
  });

  after(async function() {
    if(!DEBUG) await this.driver.quit();
  });

  describe('Software Scenarios', function() {
    it('Should allow creation of an issue', async function() {
      this.timeout(60000)
      const { driver } = this;

      await driver.get(JIRA_BASE_URL);

      await loginToJiraAsTestUser(driver);

      const createButton = await driver.findElement(selectors.NAV_CREATE_ISSUE);
      await createButton.click();

      await driver.wait(until.titleContains('Create Issue'), 10000, 'issue creation dialogue never became available');

      const summary = await driver.findElement(selectors.CREATE_ISSUE_SUMMARY_INPUT);
      await summary.sendKeys(`Test Issue ${TEST_NONCE}`);

      const submit = await driver.findElement(selectors.CREATE_ISSUE_SUBMIT_BUTTON);
      await submit.click();

      await driver.wait(until.elementLocated(selectors.POPUP_SUCCESS), 10000, 'success dialogue never popped');

      const quickSearchInput = driver.findElement(selectors.NAV_QUICK_SEARCH);
      await quickSearchInput.sendKeys(`${TEST_NONCE}`);

      const ticketSearchResult = await driver.wait(until.elementLocated(selectors.NAV_QUICK_SEARCH_RESULT_WITH_TITLE(`Test Issue ${TEST_NONCE}`)), 10000, 'created issue did not appear in search results');
      await ticketSearchResult.click();

      const ticketTitle = await driver.wait(until.elementLocated(selectors.ISSUE_VIEW_SUMMARY), 10000, 'Did not get to ticket page');
      const ticketTitleText = await ticketTitle.getText();
      expect(ticketTitleText).to.equal(`Test Issue ${TEST_NONCE}`);
    });

    it('Should be able to open an existing issue', async function() {
      this.timeout(30000);
      const { driver } = this;

      await driver.get(JIRA_BASE_URL);

      await loginToJiraAsTestUser(driver);

      //KT-16 is the last issue created in the sample project
      await driver.get(`${JIRA_BASE_URL}/browse/KT-16`)

      const title = await driver.wait(until.elementLocated(selectors.ISSUE_VIEW_SUMMARY), 10000, 'never got to issue');
      const titleText = await title.getText();

      expect(titleText).to.include('Issues like this one that are marked as fixed in a released version do not show up in Work mode but are included in the reports');
    });

    it('Should show boards', async function() {
      return this.skip();
      this.timeout(30000);
      const { driver } = this;

      await driver.get(JIRA_BASE_URL);

      await loginToJiraAsTestUser(driver);

      await navigateToKTBoard(driver);

      const backlogIssue = await driver.findElement(selectors.KANBAN_BOARD_CARD_SUMMARY_WITH_TITLE(`Test Issue ${TEST_NONCE}`), 10000, 'Test issue created earlier not present');

      expect(backlogIssue).to.be.ok;
    });

    it('Should transition an issue when its workflow is updated', async function() {
      return this.skip();
      this.timeout(30000);
      const { driver } = this;

      await driver.get(JIRA_BASE_URL);

      await loginToJiraAsTestUser(driver);

      const issueDropDown = await driver.findElement(selectors.NAV_ISSUES);
      await issueDropDown.click();
      await driver.wait(until.elementLocated(selectors.NAV_ISSUES_DROPDOWN), 3000, 'Drop down never rendered');

      const issues = await driver.findElements(selectors.NAV_ISSUES_DROPDOWN_ELEMENT);
      // Click on our test issue
      for (let i = 0; i < issues.length; i++) {
        const issue = issues[i];
        const issueTitle = await issue.getText();
        if (issueTitle.includes(TEST_NONCE)) {
          await issue.click();
          break;
        }
      }

      const workflowDropDown = await driver.wait(until.elementLocated(selectors.ISSUE_VIEW_WORKFLOW_DROPDOWN), 10000, 'Workflow dropdown never rendered');
      await workflowDropDown.click();

      const inProgressButton = await driver.findElement(selectors.ISSUE_VIEW_WORKFLOW_DROPDOWN_IN_PROGRESS);
      inProgressButton.click();

      await driver.wait(until.elementLocated(selectors.POPUP_SUCCESS), 10000, 'success dialogue never popped');

      const issueStatus = await driver.findElement(selectors.ISSUE_VIEW_STATUS);
      const issueStatusText = await issueStatus.getText();

      expect(issueStatusText).to.equal('IN PROGRESS');
    });
  });

  describe('Issue navigation scenarios', function() {
    it('Should display an issue when you search for it by issue key in the search bar', async function() {
      this.timeout(15000);
      const { driver } = this;

      await driver.get(JIRA_BASE_URL);

      await loginToJiraAsTestUser(driver);

      const quickSearchInput = driver.findElement(selectors.NAV_QUICK_SEARCH);
      await quickSearchInput.sendKeys('KT-2');
      await quickSearchInput.sendKeys(Key.ENTER);

      await driver.wait(until.titleContains('[KT-2]'), 7000, 'KT-2 never loaded');
    });

    it('Should return matching tickets when searched for with JQL', async function() {
      this.timeout(40000);
      const { driver } = this;

      await driver.get(JIRA_BASE_URL);

      await loginToJiraAsTestUser(driver);

      const urlEncodedQuery = 'project%20%3D%20"Kanban%20Test"%20AND%20text%20~%20"highlight%20delays"';
      await driver.get(`${JIRA_BASE_URL}/browse/KT-16?jql=${urlEncodedQuery}`)

      await new Promise(resolve => setTimeout(resolve, 5000));

      const searchResults = await driver.findElements(selectors.ISSUE_SEARCH_RESULT);
      expect(searchResults.length).to.equal(1);

      const issueKeyLink = await driver.findElement(selectors.ISSUE_SEARCH_RESULT_ISSUE_KEY);
      const issueKey = await issueKeyLink.getText();
      expect(issueKey).to.equal('KT-6');
    });

    it('should show issues I have created in the "reported by me" existing filter', async function() {
      this.timeout(15000);
      const { driver } = this;

      await driver.get(JIRA_BASE_URL);

      await loginToJiraAsTestUser(driver);

      const issueDropDown = driver.findElement(selectors.NAV_ISSUES);
      await issueDropDown.click();
      await driver.wait(until.elementLocated(selectors.NAV_ISSUES_DROPDOWN), 3000, 'Drop down never rendered');

      const reportedByMeLink = await driver.wait(until.elementLocated(selectors.NAV_ISSUES_DROPDOWN_REPORTED_BY_ME), 3000, '"Search for issues" button never rendered');
      await reportedByMeLink.click()

      // There are two different issue list views depending on viewport so check for both
      try {
        await driver.wait(until.elementLocated(selectors.ISSUE_SEARCH_RESULTS), 8000, 'Never reached issue list page');
        const myIssue = await driver.findElement(selectors.ISSUE_SEARCH_RESULT_WITH_ISSUE_TITLE(`Test Issue ${TEST_NONCE}`));
        expect(myIssue).to.be.ok;
      } catch(err) {
        console.log('\t\tDid not get to issue list page, probably in small view. Trying different DOM query');
        const myIssue = await driver.findElement(selectors.ISSUE_SEARCH_ALT_RESULT_WITH_ISSUE_KEY('KT-17'));
        expect(myIssue).to.be.ok;
      }
    });
  });

  describe('Plugin Scenarios', function() {
    it('Should be able to download plugins from the marketplace', async function() {
      this.timeout(120000);
      const { driver } = this;

      await driver.get(JIRA_BASE_URL);

      await loginToJiraAsAdminUser(driver);

      await navigateToPluginManager(driver);

      const pluginSearchBar = await driver.wait(until.elementLocated(selectors.UPM_SEARCH_BOX), 5000, 'Failed to reach plugin manager page');
      await pluginSearchBar.sendKeys('Portfolio for jira');
      await pluginSearchBar.sendKeys(Key.ENTER);

      const mask = await driver.findElement(selectors.UI_MASK);
      await driver.wait(until.elementIsNotVisible(mask));

      const portfolioCard = await driver.wait(until.elementLocated(selectors.UPM_SEARCH_RESULT_PORTFOLIO), 5000, 'Portfolio for Jira did not appear in search results');
      const trialButton = await portfolioCard.findElement(selectors.UPM_SEARCH_RESULT_PORTFOLIO_TRIAL_BUTTON);
      await trialButton.click();

      const successDialog = await driver.wait(until.elementLocated(selectors.UPM_POPUP), 120000, 'Download did not complete in 2m or was never started');
      const dialogHeader = await successDialog.findElement(selectors.UPM_SUCCESS_DIALOGUE);
      const headerText = await dialogHeader.getText();;
      expect(headerText).to.equal('Free trial');
    });

    it('Should be able to uninstall plugins that were downloaded from the marketplace', async function() {
      this.timeout(120000);
      const { driver } = this;

      await driver.get(JIRA_BASE_URL);

      await loginToJiraAsAdminUser(driver);

      await navigateToPluginManager(driver);

      const appManagerButton = await driver.findElement(selectors.UPM_MANAGE_APPS);
      await appManagerButton.click();

      const searchInstalledApps = await driver.wait(until.elementLocated(selectors.UPM_MANAGE_APPS_SEARCH), 8000, 'Manage apps page never rendered');

      // This is a hack because the placeholder text doesn't disappear
      await searchInstalledApps.click();
      await searchInstalledApps.sendKeys(Key.RIGHT);
      await searchInstalledApps.sendKeys(Key.RIGHT);
      for(let i = 0; i < 20; i++) {
        await searchInstalledApps.sendKeys(Key.BACK_SPACE);
      }

      await searchInstalledApps.sendKeys('Portfolio for jira');
      await searchInstalledApps.sendKeys(Key.ENTER);

      const portfolioMatchedPlugins = await driver.wait(until.elementsLocated(selectors.UPM_SEARCH_RESULT_PORTFOLIO_EXPANDER), 10000, 'Plugins did not load');
      expect(portfolioMatchedPlugins.length).to.equal(1);
      await portfolioMatchedPlugins[0].click();

      const uninstallButton = await driver.wait(until.elementLocated(selectors.UPM_UNINSTALL_BUTTON), 20000, 'Could not find plugin uninstall buton');
      await uninstallButton.click();

      const confirmUninstall = await driver.wait(until.elementLocated(selectors.UPM_UNINSTALL_DIALOG_CONFIRM), 10000, 'Confirm uninstall never appeared');
      await confirmUninstall.click();

      await driver.wait(until.elementLocated(selectors.UPM_PLUGIN_FEEDBACK_DIALOG), 20000, 'Uninstall did not complete');

      const portfolioPluginContainer = await driver.wait(until.elementLocated(selectors.UPM_SEARCH_RESULT_PORTFOLIO), 10000, 'Plugins did not load');
      
      const uninstallCompleteMessage = await portfolioPluginContainer.findElement(selectors.UPM_UNINSTALL_COMPLETE_MESSAGE);
      const uninstalledText = await uninstallCompleteMessage.getText();

      expect(uninstalledText).to.equal('This app was successfully uninstalled.');
    });
  });

  // This test has to go last. For some reason it messes with all future sessions
  describe('Admin Scenarios', function() {
    it('Should allow creation of a user', async function() {
      this.timeout(60000);
      const { driver } = this;

      await driver.get(JIRA_BASE_URL);

      await loginToJiraAsAdminUser(driver);

      await driver.get(`${JIRA_BASE_URL}/secure/admin/user/AddUser!default.jspa`);

      try{
        const confirmAdminLoginPasswordField = await driver.wait(until.elementLocated(selectors.WEBSUDO_PASSWORD_INPUT), 5000, 'Never prompted to confirm admin credentials');
        await confirmAdminLoginPasswordField.sendKeys(JIRA_ADMIN_PWD);
        const submitConfirmationLoginButton = await driver.findElement(selectors.WEBSUDO_SUBMIT_LOGIN);
        await submitConfirmationLoginButton.click();
      } catch(err) {
        console.log('Was not prompted for admin credentials. Continuing...');
      }

      const userEmailInput = driver.wait(until.elementLocated(selectors.USER_MANAGER_CREATE_USER_EMAIL_INPUT), 10000, 'User creation form never rendered');
      await userEmailInput.sendKeys(`test-${TEST_NONCE}@test.com`);

      const userFullNameInput = await driver.findElement(selectors.USER_MANAGER_CREATE_USER_FULLNAME_INPUT);
      await userFullNameInput.sendKeys(`TestUser ${TEST_NONCE}`);

      const usernameInput = await driver.findElement(selectors.USER_MANAGER_CREATE_USER_USERNAME_INPUT);
      await usernameInput.sendKeys(`test-user${TEST_NONCE}`);

      const submitButton = await driver.findElement(selectors.USER_MANAGER_CREATE_USER_SUBMIT_BUTTON);
      await submitButton.click();

      await driver.wait(until.elementLocated(selectors.POPUP_SUCCESS), 10000, 'User was not created successfully');
    });
  });
});

async function loginToJiraAsTestUser(driver) {
  await loginToJira(driver, JIRA_USER, JIRA_USER_PWD);

}

async function loginToJiraAsAdminUser(driver) {
  await loginToJira(driver, JIRA_ADMIN, JIRA_ADMIN_PWD);
}

async function loginToJira(driver, username, password) {
  await driver.get(`${JIRA_BASE_URL}`);
  const userform = await driver.wait(until.elementLocated(selectors.DASHBOARD_LOGIN_FORM_USERNAME), 10000, 'login form never rendered (username)');
  // We need to send this blank input because otherwise webdriver won't wait until the user name has been fully entered
  await userform.click();
  await userform.sendKeys('');
  await userform.sendKeys(username);
  await userform.sendKeys('');

  const passwordform = await driver.wait(until.elementLocated(selectors.DASHBOARD_LOGIN_FORM_PASSWORD), 10000, 'login form never rendered (password)');
  await passwordform.click();
  await passwordform.sendKeys(password);

  const submit = await driver.wait(until.elementLocated(selectors.DASHBOARD_LOGIN_BUTTON), 10000, 'Login button never appeared');
  await submit.click();

  await driver.wait(until.elementLocated(selectors.NAV_CREATE_ISSUE), 60000, 'login never completed');
}

async function navigateToKTBoard(driver) {
  const boardsMenuButton = await driver.findElement(selectors.NAV_BOARDS);
  await boardsMenuButton.click()

  const allBoardsButton = await driver.wait(until.elementLocated(selectors.NAV_BOARDS_DROPDOWN_ALL_BOARDS), 10000, 'Boards drop down never appeared');
  await allBoardsButton.click();

  const linkToKTBoard = await driver.wait(until.elementLocated(selectors.ALL_BOARDS_LIST_MEMBER), 10000, 'Could not find any boards in boards list');
  await linkToKTBoard.click();

  // Wait until a card is visible
  await driver.wait(until.elementLocated(selectors.KANBAN_BOARD_CARD), 10000, 'KT board never loaded');
}

async function navigateToPluginManager(driver) {
  await driver.get(`${JIRA_BASE_URL}/plugins/servlet/upm/marketplace`)

  try{
    const confirmAdminLoginPasswordField = await driver.wait(until.elementLocated(selectors.WEBSUDO_PASSWORD_INPUT), 5000, 'Never prompted to confirm admin credentials');
    await confirmAdminLoginPasswordField.sendKeys(JIRA_ADMIN_PWD);
    const submitConfirmationLoginButton = await driver.findElement(selectors.WEBSUDO_SUBMIT_LOGIN);
    await submitConfirmationLoginButton.click();
  } catch(err) {
    console.log('Was not prompted for admin credentials. Continuing...');
  }

  await driver.wait(until.elementLocated(selectors.UPM_PLUGIN_ROW), 5000, 'Failed to reach plugin manager page');
}
